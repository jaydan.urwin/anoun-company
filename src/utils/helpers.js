import { parseISO, format } from "date-fns";

export function getBlogUrl(date, slug) {
  const dateString = parseISO(date, "YYYY/MM/Do");
  const dateSegment = format(dateString, "yyyy/MM/dd");
  return `/blog/${dateSegment}/${slug}/`;
}

export function getBlogPostDate(date) {
  const dateString = parseISO(date, "YYYY/MM/Do");
  const dateSegment = format(dateString, "MMMM do, yyyy");
  return `${dateSegment}`;
}