import React from "react";
import PropTypes from "prop-types";
import { graphql, Link } from "gatsby";
import { HelmetDatoCms } from "gatsby-source-datocms";
import Img from "gatsby-image";
import Button from "@material/react-button";
import MaterialIcon from "@material/react-material-icon";
import Layout from "../components/Layout/Layout";
import Container from "../components/Container/Container";
import PostBlocks from "../components/PostBlocks/PostBlocks";
import {getBlogPostDate} from "../utils/helpers"

import styles from "./post.module.scss";

const BlogPost = ({ data }) => {

  return (
    <Layout>
      <HelmetDatoCms seo={data.post.seoMetaTags} />
      <Container>
        <div className="anoun-footer-adjust__container">
          <article className={styles.blogPostContent}>
            <Img fluid={data.post.coverImage.fluid} />
            <h1>{data.post.title}</h1>
            <div><em>{getBlogPostDate(data.post.date)}</em></div>
            <PostBlocks allBlocks={data.post.content} />
          </article>
          <Link to="/blog">
            <Button icon={<MaterialIcon icon="arrow_back" />}>Go Back</Button>
          </Link>
        </div>
      </Container>
    </Layout>
  );
};

export const postQuery = graphql`
  query($id: String!) {
    post: datoCmsPost(id: { eq: $id }) {
      title
      date
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      coverImage {
        fluid(maxWidth: 1200, imgixParams: { fm: "jpg", auto: "compress" }) {
          ...GatsbyDatoCmsSizes
        }
      }
      content {
        ... on DatoCmsTextBlock {
          model {
            apiKey
          }
          textNode {
            childMarkdownRemark {
              html
            }
          }
        }
        ... on DatoCmsYtEmbedBlock {
          model {
            apiKey
          }
          video {
            url
          }
        }
        ... on DatoCmsCodeBlock {
          model {
            apiKey
          }
          codeBlock
        }
      }
    }
  }
`;

BlogPost.propTypes = {
  data: PropTypes.object.isRequired
};

export default BlogPost;
