import React from "react";
import { graphql } from "gatsby";
import { HelmetDatoCms } from "gatsby-source-datocms";
import Layout from "../components/Layout/Layout";
import Container from "../components/Container/Container";

// Styles
import "../styles/app.scss";

const AboutPage = ({ data: { about } }) => {
  return (
    <Layout>
      <HelmetDatoCms seo={about.seoMetaTags} />
      <Container>
        <div className="anoun-footer-adjust__container">
          <h1>About Us</h1>
          <div
            dangerouslySetInnerHTML={{
              __html: about.contentNode.childMarkdownRemark.html
            }}
          />
        </div>
      </Container>
    </Layout>
  );
};

export default AboutPage;

export const query = graphql`
  query AboutQuery {
    about: datoCmsAboutPage {
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      title
      contentNode {
        childMarkdownRemark {
          html
        }
      }
    }
  }
`;
