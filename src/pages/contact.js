import React from 'react'
import Layout from '../components/Layout/Layout'
import Container from '../components/Container/Container'
import ContactForm from '../components/ContactForm/ContactForm'

// Styles
import '../styles/app.scss'

const ContactPage = () => {
  return (
    <Layout>
      <Container>
        <h1>Contact Us</h1>
        <section className="anoun-footer-adjust__container contact-page-main__section">
          <div className="anoun-contact-links__section">
            <div>
              <h3>Call/Text</h3>
              <a href="tel:+12088079333">(208) 807-9333</a>
            </div>
            <div>
              <h3>Email</h3>
              <a href="mailto:hello@anoun.company">hello@anoun.company</a>
            </div>
            <div>
              <h3>Instagram</h3>
              <a href="https://www.instagram.com/anoun.company">
                @anoun.company
              </a>
            </div>
          </div>
          <h3>or just shoot us a message here!</h3>
          <ContactForm />
        </section>
      </Container>
    </Layout>
  )
}

export default ContactPage
