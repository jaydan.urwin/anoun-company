import React from 'react'
import { Link } from 'gatsby'
import Layout from '../components/Layout/Layout'
import Container from '../components/Container/Container'
import awkwardKidGif from '../images/awkward-kid.gif'
import Button from '@material/react-button'
import MaterialIcon from "@material/react-material-icon"
// Styles
import '../styles/app.scss'

const NotFoundPage = () => (
  <Layout>
    <Container>
      <img src={awkwardKidGif} width="300px" alt="awkward kid gif" />
      <h1>Uh...this is awkward</h1>
      <p>
        Uh oh! It looks like the place you were looking for is no longer here or
        there's a typo. Please check the URL for mistakes and try again.{' '}
        <span role="img" aria-label="wink">
          😉
        </span>
      </p>
      <Link to="/">
      <Button icon={<MaterialIcon icon="arrow_back" />}>Go Back Home</Button>
      </Link>
    </Container>
  </Layout>
)

export default NotFoundPage
