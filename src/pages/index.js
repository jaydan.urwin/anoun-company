import React from "react";
import { Link } from "gatsby";
import Fab from "@material/react-fab";
import MaterialIcon from "@material/react-material-icon";
import Layout from "../components/Layout/Layout";
import Container from "../components/Container/Container";
// Styles
import "../styles/app.scss";
import anounLogoWhite from '../images/anoun-logo-white.svg'
import designIllustration from "../images/drawkit-drawing-man-colour.svg";
import developIllustration from "../images/drawkit-developer-man-colour.svg";
import deliverIllustration from "../images/drawkit-shipping-package-colour.svg";
import WorkCardGrid from "../components/WorkCardGrid/WorkCardGrid";
import fabMessageIcon from "../images/icons/icons8-telegram-app.svg"

const IndexPage = () => {
  return (
    <Layout>
      <section className="anoun-home__section anoun-home__section--hero">
        <img src={anounLogoWhite} className="anoun-title--hero fade-in" alt="ANOUN logo"/>
      </section>
      <Link to={"/contact/"}>
        <Fab
          className="app-fab--absolute"
          icon={<img className="material-icons" src={fabMessageIcon}/>}
        />
      </Link>
      <section className="anoun-home__section anoun-home__section--why">
        <Container>
          <h3>We are in the business of making successful businesses</h3>
          <h3>
            Our purpose is to take you from you’re A <s>to your</s> B.
          </h3>
          <h3>Our Services</h3>
          <ul>
            <li>Business Consulting</li>
            <li>Web Design</li>
            <li>Web Development</li>
            <li>App Design (iOS and Android)</li>
            <li>Brand Design</li>
            <li>Social Media Marketing</li>
            <li>Blogging</li>
            <li>SEO</li>
          </ul>
        </Container>
      </section>
      <section className="anoun-home-services__section">
        <Container>
          <h2>Our Process</h2>
        </Container>
        <div className="anoun-home-services__container anoun-home-services__container--reversed">
          <div className="anoun-home-services--text__container">
            <h2>Design</h2>
            <h4>Graphic, Brand, &amp; Web Design</h4>
            <p>
              No company is the same which is why we don’t take the cookie
              cutter approach when designing for your company. We want to
              partner with you to capture your vision and bring your personality
              to whatever we're designing whether it be a brand, an app, or a
              website.
            </p>
          </div>
          <div className="anoun-home-services--image__container">
            <img
              src={designIllustration}
              alt="illustration of someone designing"
            />
          </div>
        </div>

        <div className="anoun-home-services__container">
          <div className="anoun-home-services--text__container">
            <h2>Develop</h2>
            <h4>Web, Android, &amp; iOS Development</h4>
            <p>
              Excellence is our standard not our goal. You can expect anything
              we make to be fast, efficient, and top-notch. In other words we
              don't make{" "}
              <span role="img" aria-label="poop emoji">
                💩
              </span>
              . Also, our technology choices aren't from the last century. We
              use the latest, fastest, and most cutting edge technologies
              available to build, deploy, and maintain your digital presence.
            </p>
          </div>
          <div className="anoun-home-services--image__container">
            <img
              src={developIllustration}
              alt="illustration of someone typing on a computer"
            />
          </div>
        </div>
        <div className="anoun-home-services__container anoun-home-services__container--reversed">
          <div className="anoun-home-services--text__container">
            <h2>Deliver</h2>
            <h4>Social Media Marketing</h4>
            <p>
              Managing social media accounts isn't an easy task, especially when
              you have a million other things to do as a business owner. So let
              us help! We not only run your social media accounts but make it
              our goal to grow your following and push more customers to your
              doorstep.
            </p>
          </div>
          <div className="anoun-home-services--image__container">
            <img
              src={deliverIllustration}
              alt="illustration of a package being delivered"
            />
          </div>
        </div>
      </section>
      <section className="anoun-home-work__section">
        <Container>
          <h2>Our Work</h2>
        </Container>
        <WorkCardGrid />
      </section>
    </Layout>
  );
};

export default IndexPage;
