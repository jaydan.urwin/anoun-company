import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/Layout/Layout";
import Container from "../components/Container/Container";
import BlogPostPreviewCard from "../components/BlogPostPreviewCard/BlogPostPreviewCard"

// Styles
import "../styles/app.scss";

const BlogPage = ({ data }) => {
  return (
    <Layout>
      <Container>
        <div className="anoun-footer-adjust__container">
        <h1>Blog Posts</h1>
        <div className="blog-posts__container">
          {data.allDatoCmsPost.edges.map(({ node: post }) => (
          <BlogPostPreviewCard {...post}/>
          ))}
        </div>
        </div>
      </Container>
    </Layout>
  );
};

export default BlogPage;

export const query = graphql`
  query BlogQuery {
    allDatoCmsPost(sort: { fields: [position], order: ASC }) {
      edges {
        node {
          id
          title
          date
          slug
          excerpt
          coverImage {
            fluid(maxWidth: 512, imgixParams: { fm: "jpg", auto: "compress" }) {
              ...GatsbyDatoCmsSizes
            }
          }
        }
      }
    }
  }
`;
