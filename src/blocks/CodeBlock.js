import React from 'react'
import PropTypes from 'prop-types'


const CodeBlock = ({ input }) => <pre><code>{input.codeBlock}</code></pre>

export default CodeBlock

CodeBlock.propTypes = {
  input: PropTypes.object.isRequired,
}