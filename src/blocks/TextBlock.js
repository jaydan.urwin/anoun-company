import React from 'react'
import PropTypes from 'prop-types'

const TextBlock = ({ input }) => <div dangerouslySetInnerHTML={{ __html: input.textNode.childMarkdownRemark.html }} />

export default TextBlock

TextBlock.propTypes = {
  input: PropTypes.object.isRequired,
}