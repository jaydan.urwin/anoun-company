import React from 'react'
import PropTypes from 'prop-types'
import YouTubeEmbed from '../components/YouTubeEmbed/YouTubeEmbed';


const YouTubeEmbedBlock = ({ input }) => <YouTubeEmbed url={input.video.url } />

export default YouTubeEmbedBlock

YouTubeEmbedBlock.propTypes = {
  input: PropTypes.object.isRequired,
}