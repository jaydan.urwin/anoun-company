import React, { Component } from "react";
import { Link } from "gatsby";
import TopAppBar, {
  TopAppBarIcon,
  TopAppBarRow,
  TopAppBarSection
} from "@material/react-top-app-bar";
import Drawer from "@material/react-drawer";
import MaterialIcon from "@material/react-material-icon";

class Header extends Component {
  state = { open: false };
  render() {
    const ListLink = props => (
      <li>
        <Link to={props.to} activeClassName="create-header__link--active">
          {props.children}
        </Link>
      </li>
    );
    return (
      <>
        <Drawer
          modal
          open={this.state.open}
          onClose={() => this.setState({ open: false })}
        >
          <div className="create-drawer-content__container">
            <Link to={"/"}>
              <h1 className="anoun-title">ANOUN</h1>
            </Link>
            <nav>
              <ul>
                {/* <ListLink to={`/messages/`}>Messages</ListLink> */}
                <ListLink to={`/about/`}>About</ListLink>
                <ListLink to={`/blog/`}>Blog</ListLink>
                <ListLink to={`/contact/`}>Contact</ListLink>
              </ul>
            </nav>
          </div>
        </Drawer>
        <TopAppBar>
          <TopAppBarRow>
            <TopAppBarSection align="start">
              <TopAppBarIcon
                navIcon
                tabIndex={0}
                className="create-header-drawer__button"
                onClick={() => this.setState({ open: !this.state.open })}
              >
                <MaterialIcon
                  hasRipple
                  icon="menu"
                  onClick={() => console.log("click")}
                />
              </TopAppBarIcon>
              <Link to={"/"}>
                <h1 className="anoun-title">A</h1>
              </Link>
            </TopAppBarSection>
            <TopAppBarSection align="end" role="toolbar">
              <nav className="create-header__nav--desktop">
                <ul>
                  <ListLink to={`/about/`}>About</ListLink>
                  <ListLink to={`/blog/`}>Blog</ListLink>
                  <ListLink to={`/contact/`}>Contact</ListLink>
                </ul>
              </nav>
            </TopAppBarSection>
          </TopAppBarRow>
        </TopAppBar>
      </>
    );
  }
}

export default Header;
