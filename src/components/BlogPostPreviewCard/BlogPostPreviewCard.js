import React from "react";
import { Link } from "gatsby";
import Img from "gatsby-image";
import Card from "@material/react-card";
import { getBlogUrl } from "../../utils/helpers";

function BlogPostPreviewCard(props) {
  return (
    <Link to={getBlogUrl(props.date, props.slug)}>
      <Card key={props.id} className="mdc-card--clickable anoun-blog-card">
        <Img fluid={props.coverImage.fluid} className="mdc-card__media" />
        <div className="anoun-blog-card-content__container">
          <h3>{props.title}</h3>
          <p>{props.excerpt}</p>
        </div>
      </Card>
    </Link>
  );
}

export default BlogPostPreviewCard;
