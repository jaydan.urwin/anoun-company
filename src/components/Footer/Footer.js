import { Link } from "gatsby";
import React, { Component } from "react";
import instagramIcon from "../../images/icons/instagram-glyph-24px.svg";
import youtubeIcon from "../../images/icons/youtube-icon-24px.svg";
import githubIcon from "../../images/icons/github-icon-24px.svg";

import footerLogo from "../../images/ANOUN-footer-logo.png";

import styles from "./footer.module.scss";

class Footer extends Component {
  render() {
    return (
      <footer className={styles.siteFooter}>
        <nav className={styles.footerNav}>
          {/* LOGO */}
          <img
            src={footerLogo}
            width="100px"
            alt="ANOUN-logo"
            className={styles.footerLogo}
          />
          <div className={styles.footerSocialLinks}>
            <a
              href="https://www.instagram.com/anoun.company"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                src={instagramIcon}
                className={styles.footerIcon}
                alt="instagram-social-link"
              />
            </a>
            <a
              href="https://www.youtube.com/channel/UCr6HXKDblu-_gxDxCZMbRoQ?view_as=subscriber"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                src={youtubeIcon}
                className={styles.footerIcon}
                alt="youtube-social-link"
              />
            </a>
            <a
              href="https://github.com/anoun"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                src={githubIcon}
                className={styles.footerIcon}
                alt="github-social-link"
              />
            </a>
          </div>

          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/blog/">Blog</Link>
            </li>
            <li>
              <Link to="/about/">About</Link>
            </li>
            <li>
              <Link to="/contact/">Contact</Link>
            </li>
          </ul>
        </nav>
      </footer>
    );
  }
}

export default Footer;
