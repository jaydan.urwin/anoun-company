import React from 'react'
import getYouTubeID from 'get-youtube-id'

import './YouTubeEmbed.scss'

function YouTubeEmbed(props) {
  const id = getYouTubeID(props.url)
  const youtubeLink = `https://www.youtube.com/embed/${id}`
  if (!id) {
    return <div>Missing YouTube URL</div>
  }
  return (
    <div className="responsive-yt__container">
      <iframe
        title="YouTube Video"
        src={youtubeLink}
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
        loading="lazy"
      />
    </div>
  )
}

export default YouTubeEmbed
