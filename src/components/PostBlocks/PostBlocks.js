import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { TextBlock, CodeBlock, YouTubeEmbedBlock } from '../../blocks'

export default class PostBlocks extends Component {
  render() {
    const { allBlocks } = this.props
    const block = allBlocks.map(b => {
      switch (b.model.apiKey) {
        // These are the API IDs of the slices
        case 'text_block':
          return <TextBlock key={b.id} input={b} />
        case 'code_block':
          return <CodeBlock key={b.id} input={b} />
        case 'yt_embed_block':
          return <YouTubeEmbedBlock key={b.id} input={b} />
        default:
          return null
      }
    })
    return <div>{block}</div>
  }
}

PostBlocks.propTypes = {
  allBlocks: PropTypes.array.isRequired,
}
