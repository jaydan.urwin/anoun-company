module.exports = {
  title: `ANOUN`, // Navigation and Site Title
  siteUrl: `https://anoun.company`, // Domain of your site. No trailing slash!
  // image: "/", // Used for SEO

  // Manifest
  icon: `src/images/favicon.png`, // Used for manifest favicon generation
  short_name: `ANOUN`, // shortname for manifest. MUST be shorter than 12 characters
  owner: "ANOUN", // Author
  theme_color: `#000000`,
  background_color: `#ffffff`,

  // Social and Analytics
  googleAnalyticsID: `UA-122654359-2`,
}
