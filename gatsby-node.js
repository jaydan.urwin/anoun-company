const path = require("path");

const { parseISO, format } = require("date-fns");

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  // eslint-disable-next-line
  const createBlogPosts = new Promise((resolve, reject) => {
    try {
      graphql(`
        {
          allDatoCmsPost {
            nodes {
              id
              date
              slug
            }
          }
        }
      `).then(res => {
        const posts = res.data.allDatoCmsPost.nodes;
        posts.map(post => {
          let { id, date, slug } = post;
          const dateString = parseISO(date, "YYYY/MM/Do");
          const dateSegment = format(dateString, "yyyy/MM/dd");
          createPage({
            path: `/blog/${dateSegment}/${slug}/`,
            component: path.resolve("./src/templates/post.js"),
            context: {
              id
            }
          });
        });
        resolve();
      });
    } catch (error) {
      reject(error);
    }
  });

  // eslint-disable-next-line
  return Promise.all([createBlogPosts]);
};
